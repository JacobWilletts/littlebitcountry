export default {
  plugins: [{
    src: '~/plugins/GoogleAnalytics',
    mode: 'client',
  }],
  head: {
    titleTemplate: 'Wooden Furniture Hire for Weddings & Events in the Waikato',
    meta: [{
        charset: 'utf-8'
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },

      // hid is used as unique identifier. Do not use `vmid` for it as it will not work
      {
        hid: 'description',
        name: 'description',
        content: 'Wooden furniture hire for weddings & events. Our collection includes trestle tables, bench seats, cake tables and chalkboards. Based near Hamilton and supplying to the Waikato, South Auckland, Coromandel and Bay of Plenty.'
      }
    ],
    script: [{
      src: "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js",
      type: "text/javascript"
    }],
    link: [{
      rel: 'icon',
      type: 'image/x-icon',
      href: 'favicon.ico'
    }]
  }
}