# littlebitcountry

Static website for advertising hire furniture. This is a Nuxt (NodeJS) SPA with prerendering enabled (for SEO), built and deployed with Bitbucket pipelines. This allows for static hosting (e.g. AWS S3) with no cost.